package com.mobila.crisart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrisartApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrisartApplication.class, args);
	}

}

