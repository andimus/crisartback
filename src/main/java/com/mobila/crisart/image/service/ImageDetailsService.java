package com.mobila.crisart.image.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.mobila.crisart.image.model.ImageDetails;
import com.mobila.crisart.image.repository.ImageDetailsRepository;

@Service
public class ImageDetailsService {
	
	@Autowired
	private ImageDetailsRepository repository;
	
	@Value("${file.upload-dir}")
	private String uploadDirectory;
	
	//Find all images
	public List<ImageDetails> findImageDetails() {
		return this.repository.findAll();
	}
	
	//Find all images by Category Id
	public List<ImageDetails> findAllByImageCategoryId(Long id) {
		return this.repository.findAllByImageCategoryId(id);
	}
	
	//Save New Images[]
	public void saveImage(MultipartFile[] images, String imgDetailsStr) {
		Gson gson = new Gson();
		for(MultipartFile image : images) {
			try {
				ImageDetails imageDetails = gson.fromJson(imgDetailsStr, ImageDetails.class);
				imageDetails.setImageName(image.getOriginalFilename());
				File fileImage = new File(this.uploadDirectory + image.getOriginalFilename());
				image.transferTo(fileImage);
				this.repository.save(imageDetails);
			} catch (IllegalStateException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	//Update category of image
	public void updateCategoryOfImage(ImageDetails image) {
		this.repository.findById(image.getId()).ifPresent(s -> {
			s.setImageCategory(image.getImageCategory());
			this.repository.save(s);
		});
	}
	
	// delete img by id
	public void deleteImageById(Long imgId) {
		System.out.println(imgId);
		this.repository.deleteById(imgId);
	}

}
