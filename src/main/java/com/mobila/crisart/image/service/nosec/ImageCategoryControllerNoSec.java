package com.mobila.crisart.image.service.nosec;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mobila.crisart.image.model.ImageCategory;
import com.mobila.crisart.image.service.ImageCategoryService;

@CrossOrigin("*")
@RestController
@RequestMapping("/crisart/noSec")
public class ImageCategoryControllerNoSec {
	
	@Autowired
	private ImageCategoryService service;
	
	@RequestMapping("/all")
	public List<ImageCategory> getAllImageCategory() {
		return this.service.findAllImageCategory();
	}

}
