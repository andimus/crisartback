package com.mobila.crisart.image.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
//import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobila.crisart.image.model.ImageCategory;
import com.mobila.crisart.image.model.interf.CategoryForMenu;
import com.mobila.crisart.image.model.interf.CategoryIdAndName;
import com.mobila.crisart.image.repository.ImageCategoryRepository;

@Service
public class ImageCategoryService {

	@Autowired
	private ImageCategoryRepository repository;
	
	@Value("${image.upload-category}")
	private String imageCategoryPath;
	
	public List<ImageCategory> findAllImageCategory() {
		return this.repository.findAllByOrderByIdAsc();
	}
	
	public List<CategoryIdAndName> findTypeNameROAndIdOrderByTypeNameRO() {
		return this.repository.findTypeNameROAndIdOrderByTypeNameRO();
	}
	
	public void saveImageCategory(ImageCategory categoryName) {
		this.repository.save(categoryName);
	}
	
	public void updateImageCategory(ImageCategory category) {
//		ImageCategory cat = new ObjectMapper().readValue(src, valueType)
		this.repository.findById(category.getId()).ifPresent(s -> {
			s.setTypeNameRO(category.getTypeNameRO());
			s.setTypeNameRU(category.getTypeNameRU());
			this.repository.save(s);
		});
	}
	
	public void deleteCategoryById(Long id) {
			this.repository.deleteById(id);
	}
	
	public void updateImageCategoryWithImage(MultipartFile file, String category) {
		
		Gson gson = new Gson();
		ImageCategory categ = gson.fromJson(category, ImageCategory.class);
		
		
		this.repository.findById(categ.getId()).ifPresent(s -> {
			s.setTypeNameRO(categ.getTypeNameRO());
			s.setTypeNameRU(categ.getTypeNameRU()); 
			s.setCategoryNumber(categ.getCategoryNumber());if(file != null) {
				categ.setImageCategoryPath(file.getOriginalFilename());
				System.out.println("File for upload = " + file.getOriginalFilename());
				this.uploadImage(file);
				s.setImageCategoryPath(categ.getImageCategoryPath());
			}
			this.repository.save(s); 
		});
	}
	
	public void uploadImage(MultipartFile uploadedFiles) {
		
		String newUploadDirectory = this.imageCategoryPath + "/"; 
		File dirFile = new File(newUploadDirectory);
		if(!dirFile.exists()) {
			System.out.println(dirFile.exists());
			boolean check = dirFile.mkdir();
			System.out.println(check);
		} 
			try {
				System.out.println(newUploadDirectory);
				File file = new File(newUploadDirectory + uploadedFiles.getOriginalFilename());
				uploadedFiles.transferTo(file);
			} catch (IllegalStateException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace(); 
			}
		
	}
	
	public List<String> getAllCategoryByLanguage(String language) {
		if(language.equalsIgnoreCase("ru")) {
			return this.repository.findAllTypeNameRU();
		} else {
			return this.repository.findAllTypeNameRO();
		}
	}
	
	public List<ImageCategory> findAllByOrderByCategoryNumberAsc() {
		return this.repository.findAllByOrderByCategoryNumberAsc();
	} 
	
	public List<CategoryForMenu> findCategoryForMenu(){
		return this.repository.findCategoryForMenu();
	}
	
	
}
