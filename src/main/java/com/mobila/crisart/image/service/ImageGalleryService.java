package com.mobila.crisart.image.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.mobila.crisart.image.model.ImageGallery;
import com.mobila.crisart.image.repository.ImageGalleryRepository;

@Service
public class ImageGalleryService {

	@Autowired
	private ImageGalleryRepository repository;

	public void saveImage(MultipartFile[] images, String imgDetailsStr) {
		Gson gson = new Gson();
		for (MultipartFile image : images) {
			try {
				ImageGallery imageGallery = gson.fromJson(imgDetailsStr, ImageGallery.class);
				imageGallery.setImage(image.getBytes());
				this.repository.save(imageGallery);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}
	
	public List<ImageGallery> allImageGallery() {
		return this.repository.findAll();
	}

}
