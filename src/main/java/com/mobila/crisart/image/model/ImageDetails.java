package com.mobila.crisart.image.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ImageDetails {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String imageName;
//	private String filePath;
	
	@ManyToOne
	@JoinColumn(name="imageCategory")
	private ImageCategory imageCategory; 
	
	public ImageDetails() {
		super();
	}

	public ImageDetails(String imageName, String filePath) {
		super();
		this.imageName = imageName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public ImageCategory getImageCategory() {
		return imageCategory;
	}

	public void setImageCategory(ImageCategory imageCategory) {
		this.imageCategory = imageCategory;
	}
	
}
