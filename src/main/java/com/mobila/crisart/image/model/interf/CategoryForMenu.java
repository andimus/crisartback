package com.mobila.crisart.image.model.interf;

public interface CategoryForMenu {
	Long getId();
	String getTypeNameRO();
	String getTypeNameRU();
}
