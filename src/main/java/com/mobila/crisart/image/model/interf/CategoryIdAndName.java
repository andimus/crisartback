package com.mobila.crisart.image.model.interf;

public interface CategoryIdAndName {
	Long getId();
	String getTypeNameRO();
}
