package com.mobila.crisart.image.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class ImageCategory {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String typeNameRO;
	
	private String typeNameRU;
	
	private String imageCategoryPath;
	
	private int categoryNumber;
	
	@OneToMany(mappedBy="imageCategory", cascade=CascadeType.ALL, orphanRemoval=true)
	@JsonIgnore
	private List<ImageDetails> imageList;

	public ImageCategory() {
		super();
	}
	
	public ImageCategory(Long id, String typeNameRO) {
		super();
		this.id = id;
		this.typeNameRO = typeNameRO;
	}

	public ImageCategory(String typeNameRO, String typeNameRU) {
		super();
		this.typeNameRO = typeNameRO;
		this.typeNameRU = typeNameRU;
	}
	
	public ImageCategory(Long id, String typeNameRO, String typeNameRU) {
		super();
		this.id = id;
		this.typeNameRO = typeNameRO;
		this.typeNameRU = typeNameRU;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public List<ImageDetails> getImageList() {
		return imageList;
	}

	public void setImageList(List<ImageDetails> imageList) {
		this.imageList = imageList;
	}
	
	public String getImageCategoryPath() {
		return imageCategoryPath;
	}

	public void setImageCategoryPath(String imageCategoryPath) {
		this.imageCategoryPath = imageCategoryPath;
	}

	public String getTypeNameRO() {
		return typeNameRO;
	}

	public void setTypeNameRO(String typeNameRO) {
		this.typeNameRO = typeNameRO;
	}

	public String getTypeNameRU() {
		return typeNameRU;
	}

	public void setTypeNameRU(String typeNameRU) {
		this.typeNameRU = typeNameRU;
	}

	public int getCategoryNumber() {
		return categoryNumber;
	}

	public void setCategoryNumber(int categoryNumber) {
		this.categoryNumber = categoryNumber;
	}
	
}
