package com.mobila.crisart.image.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity
public class ImageGallery {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String imageName;
	
	@Lob
	private byte[] image;
	
	@ManyToOne
	@JoinColumn(name="imageCategory")
	private ImageCategory imageCategory;

	public ImageGallery(String imageName, byte[] image, ImageCategory categoryList) {
		super();
		this.imageName = imageName;
		this.image = image;
	}

	public ImageGallery() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public ImageCategory getImageCategory() {
		return imageCategory;
	}

	public void setImageCategory(ImageCategory imageCategory) {
		this.imageCategory = imageCategory;
	}

}
