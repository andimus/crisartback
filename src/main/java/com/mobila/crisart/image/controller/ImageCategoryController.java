package com.mobila.crisart.image.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mobila.crisart.image.model.ImageCategory;
import com.mobila.crisart.image.model.interf.CategoryIdAndName;
import com.mobila.crisart.image.service.ImageCategoryService;

@RestController
@CrossOrigin("*")
@RequestMapping("/crisart/image/category")
public class ImageCategoryController {
	
	@Autowired
	private ImageCategoryService service;
	
	@GetMapping("/all")
	@PreAuthorize("hasRole('ADMIN')")
	public List<ImageCategory> findAllIageCategory() {
		return this.service.findAllImageCategory();
	}
	
	@GetMapping("/idAndName")
	@PreAuthorize("hasRole('ADMIN')")
	public List<CategoryIdAndName> findTypeNameROAndIdOrderByTypeNameRO() {
		return this.service.findTypeNameROAndIdOrderByTypeNameRO();
	}
	
	@PostMapping("/add")
	@PreAuthorize("hasRole('ADMIN')")
	public void saveImageCategory(@RequestBody ImageCategory imageCategory) {
		this.service.saveImageCategory(imageCategory);
	}
	
	@PutMapping("/update")
	@PreAuthorize("hasRole('ADMIN')")
	public void  updateImageCategory(@RequestBody ImageCategory imageCategory) {
		this.service.updateImageCategory(imageCategory);
//		return new ResponseEntity<>("Updated success", HttpStatus.OK);
	}
	 
	@DeleteMapping("/delete")
	@PreAuthorize("hasRole('ADMIN')")
	public void deleteCategoryById(@RequestParam("id") Long id) {
		this.service.deleteCategoryById(id);  
	}
	
	@PutMapping("/updateWithImage")
	@PreAuthorize("hasRole('ADMIN')")
	public void updateImageCategoryWithImage(@RequestParam(name = "file", required = false) MultipartFile file, @RequestParam("category") String category) {
		this.service.updateImageCategoryWithImage(file, category);
	}

}
