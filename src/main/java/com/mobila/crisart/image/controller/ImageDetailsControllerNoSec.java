package com.mobila.crisart.image.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mobila.crisart.image.model.ImageDetails;
import com.mobila.crisart.image.model.ImageGallery;
import com.mobila.crisart.image.service.ImageDetailsService;
import com.mobila.crisart.image.service.ImageGalleryService;

@CrossOrigin("*")
@RestController
@RequestMapping("/crisart/images/nosec")
public class ImageDetailsControllerNoSec {
	
	@Autowired
	private ImageDetailsService service;
	
	@Autowired
	private ImageGalleryService imgService;
	
	@GetMapping("/imageByCategory")
	public List<ImageDetails> findAllByImageCategoryId(@RequestParam("id") Long id) {
		System.out.println("\n\n\n\n\n\n" + id + "\n\n\n\n\n\n");
		return this.service.findAllByImageCategoryId(id);
	}
	
	@PostMapping("/saveImage")
	public void saveImage(@RequestParam("images") MultipartFile[] images, @RequestParam("imgDetailsStr") String imgDetailsStr) {
		this.imgService.saveImage(images, imgDetailsStr);
	}
	
	@GetMapping("/allTest")
	public List<ImageGallery> allImageGallery() {
		return this.imgService.allImageGallery();
	}
}
