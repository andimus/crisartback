package com.mobila.crisart.image.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mobila.crisart.image.model.ImageDetails;
import com.mobila.crisart.image.service.ImageDetailsService;

@RestController
@CrossOrigin("*")
@RequestMapping("/crisart/image/details")
public class ImageDetailsController {
	
	@Autowired
	private ImageDetailsService service;
	
	@GetMapping("/all")
	@PreAuthorize("hasRole('ADMIN')")
	public List<ImageDetails> findImageDetails() {
		return this.service.findImageDetails();
	}
	
	@GetMapping("/imageByCategory")
	@PreAuthorize("hasRole('ADMIN')")
	public List<ImageDetails> findAllByImageCategoryId(@RequestParam("id") Long id) {
		return this.service.findAllByImageCategoryId(id);
	}
	
	@PostMapping("/save")
	@PreAuthorize("hasRole('ADMIN')")
	public void saveImage(@RequestParam("images") MultipartFile[] images, @RequestParam("imgDetails") String imgDetailsStr) {
		this.service.saveImage(images, imgDetailsStr);
	}
	
	@PutMapping("/updateCat")
	@PreAuthorize("hasRole('ADMIN')")
	public void updateCategoryOfImage(@RequestBody ImageDetails image) {
		this.service.updateCategoryOfImage(image);
	} 
	
	@DeleteMapping("/delete")
	@PreAuthorize("hasRole('ADMIN')")
	public void deleteImageById(@RequestParam("id") Long id) {
		System.out.println(id);
		this.service.deleteImageById(id);
	}

}
