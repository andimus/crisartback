package com.mobila.crisart.image.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mobila.crisart.image.model.ImageCategory;
import com.mobila.crisart.image.model.interf.CategoryForMenu;
import com.mobila.crisart.image.service.ImageCategoryService;

@RestController
@CrossOrigin("*")
@RequestMapping("/crisart/image/category/non")
public class ImageCategoryControllerNonSecurity {
	
	@Autowired
	private ImageCategoryService service;
	
	@GetMapping("/all")
	public List<ImageCategory> findAllImageCategory(){
		return this.service.findAllImageCategory();
	}
	
//	@GetMapping("/menuCateg")
//	public List<String> getAllCategory(@RequestParam("language") String language){
//		System.out.println("Language = " + language);
//		return this.service.getAllCategoryByLanguage(language);
//	}
	
	@GetMapping("/categoryByNumber")
	public List<ImageCategory> findAllByOrderByCategoryNumberAsc() {
		return this.service.findAllByOrderByCategoryNumberAsc();
	} 
	
	@GetMapping("/menuCateg")
	public List<CategoryForMenu> findCategoryForMenu() {
		return this.service.findCategoryForMenu();
	}
	
	
}
