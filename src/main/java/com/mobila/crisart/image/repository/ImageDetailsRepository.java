package com.mobila.crisart.image.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mobila.crisart.image.model.ImageDetails;

public interface ImageDetailsRepository extends JpaRepository<ImageDetails, Long> {
	
	List<ImageDetails> findAllByImageCategoryId(Long id);
	
}
