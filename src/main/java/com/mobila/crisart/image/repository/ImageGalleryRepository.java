package com.mobila.crisart.image.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mobila.crisart.image.model.ImageGallery;


public interface ImageGalleryRepository extends JpaRepository<ImageGallery, Long> {

}
