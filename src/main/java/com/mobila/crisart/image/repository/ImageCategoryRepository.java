package com.mobila.crisart.image.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.mobila.crisart.image.model.ImageCategory;
import com.mobila.crisart.image.model.interf.CategoryForMenu;
import com.mobila.crisart.image.model.interf.CategoryIdAndName;

public interface ImageCategoryRepository extends JpaRepository<ImageCategory, Long> {
	
	List<ImageCategory> findAllByOrderByIdAsc();
	
	@Query("select i.typeNameRO from ImageCategory i")
	List<String> findAllTypeNameRO();
	
	@Query("select i.typeNameRU from ImageCategory i")
	List<String> findAllTypeNameRU();
	
	@Query("select new ImageCategory(i.id, i.typeNameRO) from ImageCategory i order by i.typeNameRO ASC")
	List<CategoryIdAndName> findTypeNameROAndIdOrderByTypeNameRO();
	
	@Query("select new ImageCategory(i.id, i.typeNameRO, i.typeNameRU) from ImageCategory i order by i.typeNameRO ASC")
	List<CategoryForMenu> findCategoryForMenu();
	
	List<ImageCategory> findAllByOrderByCategoryNumberAsc();
	
}
