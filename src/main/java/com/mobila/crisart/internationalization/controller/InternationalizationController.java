package com.mobila.crisart.internationalization.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mobila.crisart.internationalization.service.InternationalizationService;


@RestController
@RequestMapping("/messages/get")
@CrossOrigin("*")
public class InternationalizationController {

	@Autowired
	private InternationalizationService service; 
	
	@GetMapping()
	public String getMessage(@RequestParam("requiredMessage") String requiredMessage, @RequestHeader(name="Accept-Language", required=false) Locale locale) {
		return this.service.getMessage(requiredMessage, locale);
	}
	
}
