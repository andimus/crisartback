package com.mobila.crisart.internationalization.service;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

@Service
public class InternationalizationService {
	
	@Autowired
	private MessageSource messagesSource;
	
	public String getMessage(String requiredMessage, Locale locale) {
		return this.messagesSource.getMessage(requiredMessage, null, locale);
	}
}
